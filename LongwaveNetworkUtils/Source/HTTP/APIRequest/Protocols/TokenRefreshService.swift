//
//  TokenRefreshService.swift
//  NetworkUtils
//
//  Created by Massimo Cesaraccio on 12/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public protocol TokenRefreshService {
    func refreshToken<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void)
    func accessToken() -> String?
}

