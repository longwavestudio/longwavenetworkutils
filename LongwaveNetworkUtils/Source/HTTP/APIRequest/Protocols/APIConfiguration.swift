//
//  APIConfiguration.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 19/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public protocol APIConfiguration {
    var baseURL: URL { get }
}

struct APIConfigurationData: Codable, APIConfiguration {
    var baseURL: URL
}
