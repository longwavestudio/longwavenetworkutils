//
//  HttpAPIExecutor.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation
import Combine
public typealias OnApICompleted<ResponseType> = (_ completion: Result<ResponseType, Error>) -> Void

public protocol HttpAPIExecutor {
        
    /// Executes an API already registered on the system
    /// - Parameters:
    ///   - identifier: The identifier of the API to be executed.
    ///   - endpointParameters: The parameters to be set in the url.
    ///   - parameters: The parameters to be sent in the API. All parameters are coupled in the dictionary together with the key taken from the API.
    ///   - headers: The headers to be sent in the HTTP Header section.
    ///   - onCompletion: The completion block called when the API has completed
    func executeApi<ResponseType: Decodable>(identifier: String,
                                             endpointParameters: [String: String],
                                             headers: [APIHttpHeader],
                                             parameters: [String: Any],
                                             onCompletion: @escaping OnApICompleted<ResponseType>)
    
    /// Executes an API already registered on the system
    /// - Parameters:
    ///   - identifier: The identifier of the API to be executed.
    ///   - endpointParameters: The parameters to be set in the url.
    ///   - parameters: The parameters to be sent in the API. All parameters are coupled in the dictionary together with the key taken from the API.
    ///   - headers: The headers to be sent in the HTTP Header section.
    /// - returns the Future to subscribe for
    @available(iOS 13, *)
    func executeApi<ResponseType: Decodable>(identifier: String,
                                             endpointParameters: [String: String],
                                             headers: [APIHttpHeader],
                                             parameters: [String: Any]) throws -> Future<ResponseType, Error>
}
