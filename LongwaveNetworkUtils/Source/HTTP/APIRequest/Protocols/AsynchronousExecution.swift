//
//  AsynchronousExecution.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

protocol AsynchronousExecution {
    var managerQueue: DispatchQueue? { get }
    
    /// Execute asynchronously a block of code
    /// - Parameter block: the block to be executed
    func schedule(block: @escaping () -> Void)
    /// Execute synchronously a block of code
    /// - Parameter block: the block to be executed
    func execute(block: @escaping () -> Void)
}

extension AsynchronousExecution {
    func schedule(block: @escaping () -> Void) {
        guard let queue = managerQueue else {
            block()
            return
        }
        queue.async {
            block()
        }
    }
    
    func execute(block: @escaping () -> Void) {
        guard let queue = managerQueue else {
            block()
            return
        }
        queue.sync {
            block()
        }
    }
}
