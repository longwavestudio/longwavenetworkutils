//
//  HTTPRequestProvider.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation
import Combine

public struct APIHttpHeader {
    public var name: String
    public var value: String
    
    public init(name: String, value: String) {
        self.name = name
        self.value = value
    }
}

public typealias OnValidationRequested = (URLRequest?, HTTPURLResponse, Data?) -> Result<Void, Error>
public typealias OnRequestDidComplete<ResponseType: Decodable> = (_ result: Result<ResponseType, Error>) -> Void

public typealias OnConfigurationDidRetrieve = (Result<Data, Error>) -> Void

/// Based on Alamofire 5 interfaces.
/// An instance implementing HTTPRequestProvider is capable of sending http requests to the server.
public protocol HTTPRequestProvider {
        
    /// Retrieves the configuration file
    /// - Parameters:
    ///   - url: The URL where the configuration file is stored
    ///   - completion: The completion block invoked when the request is completed
    func retrieveConfiguration(completion: @escaping OnConfigurationDidRetrieve)
    
    /// Requests to the actual http framework to send the http request
    /// - Parameters:
    ///   - api: The api to be requested.
    ///   - baseURL: The api base url.
    ///   - headers: The headers to be sent.
    ///   - endpointParameters: The parameters to be replaced in query string.
    ///   - parameters: The parameters to be serialized in the request.
    ///   - completion: Executed when the API request is completed.
    func request<ResponseType: Decodable>(_ api: APIEndpoint,
                               baseURL: URL,
                               headers: [APIHttpHeader]?,
                               endpointParameters: [String: String],
                               parameters: [String: Any]?,
                               completion: @escaping OnRequestDidComplete<ResponseType>) 
    
    /// Requests to the actual http framework to send the http request
    /// - Parameters:
    ///   - api: The api to be requested.
    ///   - baseURL: The api base url.
    ///   - headers: The headers to be sent.
    ///   - endpointParameters: The parameters to be replaced in query string.
    ///   - parameters: The parameters to be serialized in the request.
    /// - Returns: the Future to subscribe for
    @available(iOS 13, *)
    func request<ResponseType: Decodable>(_ api: APIEndpoint,
                               baseURL: URL,
                               headers: [APIHttpHeader]?,
                               endpointParameters: [String: String],
                               parameters: [String: Any]?) -> Future<ResponseType, Error>
}
