//
//  HttpAPIManagerConfiguration.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public typealias OnAPIRegisteredBlock = (_ identifier: String, _ hasBeenRegistered: Bool) -> Void
public typealias OnAPIsRegisteredBlock = () -> Void

enum HttpAPIConfigurationError: Error {
    case alreadyRegistered
}

public protocol HttpAPIManagerConfiguration {
    /// Registers an API inside the Generic http manager
    /// - Parameters:
    ///   - api: the api to be registered
    ///   - onRegistered: The block called when the API is registered in the system
    /// - Note: This method should not be used in iOS 13 and above if you want to use the combine version of the execute Api. The list of api Set has to be considered as immutable.
    @available(swift, deprecated: 14.0, message: "Please migrate to registerApis if you handle only iOs 13 and later versions.")
    func registerApi(api: APIEndpoint, onRegistered: OnAPIRegisteredBlock?)
    
    /// Registers a list of APIs
    /// - Parameters:
    ///   - apis: The list of APIs to be registered
    ///   - onRegistered: The block called when the APIs are registered in the system
    /// - Note: This method should be used in iOS 13 and above if you want to use the combine version of the execute Api. The list of api Set has to be considered as immutable.
    @available(iOS 13.0, *)
    func registerApis(_ apis: [String: APIEndpoint], onRegistered: OnAPIsRegisteredBlock?)
}
