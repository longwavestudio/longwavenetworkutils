//
//  HttpAPIManager.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation
import Combine

/// Errors thrown from the api manager.
enum APIExecutionError: Error {
    /// configurationError
    /// - Thrown when the configuration API fails.
    case configurationError
    /// configurationNotReady
    /// - Thrown when the user tries to execute an API but the configuration API has not been retrieved yet.
    case configurationNotReady
    /// apiNotFound
    /// - Thrown when the user forget to register an api and asks for that API.
    case apiNotFound
    /// authenticationRequired
    /// - Thrown if an API has been executed with an expired token and requires authentication.
    case authenticationRequired
    /// parametersNotFound
    /// - Thrown if the parameters to be set in the endpoint have not been found.
    case parametersNotFound
    /// - tokenExpired
    /// - Thrown when the token is expired.
    case tokenExpired
    /// - wrongInitialization
    /// - The initialization has been done using the initializator for the Non Combine version
    case wrongInitialization
}

enum AuthenticationStatus {
    case ok
    case tokenExpired
    case tokenRequired
}

enum APIErrorCode: Int {
    case tokenExpired = 10345
}

public typealias OnReadyToSendRequestsData = (Result<Data, Error>) -> Void
public typealias OnAuthenticationTokenStored = (_ token: APIAuthentication?) -> Void

enum InitializationType {
    case combine
    case old
}

public final class HttpAPIManager {
    internal let managerQueue: DispatchQueue?
    private(set) var apis: [String: APIEndpoint] = [:]
    private let combineApis: [String: APIEndpoint]

    private var httpRequestProvider: HTTPRequestProvider
    private var configuration: APIConfiguration!
    private var initializationType: InitializationType
    /// Manager inizilization
    /// - Parameters:
    ///   - queue: The queue used to serialize requests. If not set it uses an internal queue. Does NOT guarantee the requests will call back on this thread
    ///   - requestProvider: The framework that actually implement the Http requests.
    ///   - configurationUrl: The url of the configuration file
    /// - Note: To be used when the app is not using the combine version of the api
    @available(swift, deprecated: 14.0, message: "Please migrate to init with apis list.")
    public init(queue: DispatchQueue? = DispatchQueue(label: "HttpAPIManager"),
                requestProvider: HTTPRequestProvider,
                configuration: APIConfiguration? = nil) {
        initializationType = .old
        managerQueue = queue
        httpRequestProvider = requestProvider
        self.configuration = configuration
        self.combineApis = [:]
    }
    
    /// Manager inizilization
    /// - Parameters:
    ///   - queue: The queue used to serialize requests. If not set it uses an internal queue. Does NOT guarantee the requests will call back on this thread
    ///   - requestProvider: The framework that actually implement the Http requests.
    ///   - configurationUrl: The url of the configuration file
    /// - Note: To be used when the app is  using the combine version of the api.
    @available(iOS 13.0, *)
    public init(queue: DispatchQueue? = DispatchQueue(label: "HttpAPIManager"),
                apis: [APIEndpoint],
                requestProvider: HTTPRequestProvider,
                configuration: APIConfiguration? = nil) {
        initializationType = .combine
        managerQueue = queue
        self.combineApis = apis.reduce(into: [String: APIEndpoint](), { result, newApi in
            result[newApi.apiIdentifier] = newApi
        })
        httpRequestProvider = requestProvider
        self.configuration = configuration
    }
    
    /// Retrieve configuration file given the url
    /// - Parameters:
    ///   - configurationUrl: The url of the confiuration file on the server
    ///   - onReadyToExecuteAPI: The completion block invoked when the configuration file completes
    public func retrieveConfiguration(onReadyToExecuteAPI: OnReadyToSendRequestsData?) {
        httpRequestProvider.retrieveConfiguration { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                let configurationAPI = try? JSONDecoder().decode(APIConfigurationData.self, from: data)
                guard let conf = configurationAPI else {
                    onReadyToExecuteAPI?(.failure(APIExecutionError.configurationError))
                    return
                }
                self.schedule {
                    self.configuration = conf
                    onReadyToExecuteAPI?(.success(data))
                }
            case .failure(let error):
                onReadyToExecuteAPI?(.failure(error))
            }
        }
    }
}

extension HttpAPIManager: HttpAPIManagerConfiguration {
    @available(swift, deprecated: 14.0, message: "Please register apis on init if you want to use the combine version of the execute API.")
    public func registerApi(api: APIEndpoint,
                            onRegistered: OnAPIRegisteredBlock?) {
        schedule {
            guard self.initializationType == .old else {
                assertionFailure("You have used initialization for Combine version. The list of APIs is immutable and you have to register the apis at initialization time.")
                return
            }
            let identifier = api.apiIdentifier
            guard self.apis[identifier] == nil else {
                onRegistered?(identifier, false)
                return
            }
            self.apis[identifier] = api
            onRegistered?(identifier, true)
        }
    }
    
    @available(swift, deprecated: 14.0, message: "Please register apis on init if you want to use the combine version of the execute API.")
    public func registerApis(_ apis: [String: APIEndpoint],
                             onRegistered: OnAPIsRegisteredBlock?) {
        schedule {
            guard self.initializationType == .old else {
                assertionFailure("You have used initialization for Combine version. The list of APIs is immutable and you have to register the apis at initialization time.")
                return
            }
            self.apis = apis
            onRegistered?()
        }
    }
}

extension HttpAPIManager: HttpAPIExecutor {
    @available(iOS 13.0, *)
    public func executeApi<ResponseType>(identifier: String,
                                         endpointParameters: [String : String],
                                         headers: [APIHttpHeader],
                                         parameters: [String : Any]) throws -> Future<ResponseType, Error> where ResponseType : Decodable {
        guard self.initializationType == .combine else {
            assertionFailure("You have used initialization for non Combine version. ")
            throw APIExecutionError.wrongInitialization
        }
        guard self.configuration != nil else {
            throw APIExecutionError.configurationNotReady
        }
        guard let api = self.combineApis[identifier] else {
            throw APIExecutionError.apiNotFound
        }
        let baseURL = self.configuration.baseURL
        return self.httpRequestProvider.request(api,
                                               baseURL: baseURL,
                                               headers: headers,
                                               endpointParameters: endpointParameters,
                                               parameters: parameters)
    }
    
    
    @available(swift, deprecated: 14.0, message: "Please migrate to the combine version of executeApi if you handle only iOs 13 and later versions.")
    public func executeApi<ResponseType: Decodable>(identifier: String,
                                             endpointParameters: [String: String],
                                             headers: [APIHttpHeader],
                                             parameters: [String: Any],
                                             onCompletion: @escaping OnApICompleted<ResponseType>) {
        schedule {
            guard self.initializationType == .old else {
                assertionFailure("You have used initialization for Combine version. The list of APIs is immutable and you have to register the apis at initialization time.")
                return
            }
            guard self.configuration != nil else {
                onCompletion(.failure(APIExecutionError.configurationNotReady))
                return
            }
            guard let api = self.apis[identifier] else {
                onCompletion(.failure(APIExecutionError.apiNotFound))
                return
            }
            let baseURL = self.configuration.baseURL
            self.httpRequestProvider.request(api,
                                             baseURL: baseURL,
                                             headers: headers,
                                             endpointParameters: endpointParameters,
                                             parameters: parameters,
                                             completion: onCompletion)
            
        }
    }
}

extension HttpAPIManager: AsynchronousExecution {}

/// Private methods
extension HttpAPIManager {
    /// Check endpoint parameters to check if they have been correctly set
    /// - Parameters:
    ///   - parameters: The parameters to be added to the endpoint
    ///   - api: The API definition
    private func endpoint(withParameters parameters: [String: String],
                          for api: APIEndpoint) throws -> String {
        guard let expectedParameters = api.parametersKeys else {
            return api.endPoint
        }
        guard parameters.keys.count >= expectedParameters.count else {
            throw APIExecutionError.apiNotFound
        }
        for key in expectedParameters {
            if parameters[key] == nil {
                throw APIExecutionError.apiNotFound
            }
        }
        return try api.endPointWithParameters { key in
            return parameters[key]
        }
    }
        
    /// Parse an error given the data contained inside the Http payload
    /// - Parameter data: The data containing the error description
    private func parseError(data: Data) -> ErrorData? {
        return try? JSONDecoder().decode(ErrorData.self, from: data)
    }
}

