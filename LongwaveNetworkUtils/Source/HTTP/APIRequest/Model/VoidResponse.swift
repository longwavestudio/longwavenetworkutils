//
//  VoidResponse.swift
//  NetworkUtils
//
//  Created by Massimo Cesaraccio on 13/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

/// To be used as a result type for APIs that respond with no data upon success.
public struct VoidResponse: Decodable {
    public init() {}
}

