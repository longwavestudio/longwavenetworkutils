//
//  AuthenticationToken.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 20/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

struct AuthenticationToken {
    var accessToken: String
    var refreshToken: String
}

extension AuthenticationToken: APIAuthentication {}
