//
//  ErrorData.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 19/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

/// Describes the payload contained in an Http error response
struct ErrorData: Codable {
    /// The internal error code defined by the apis
    var errorCode: Int
    /// The internal error message defined by the apis
    var errorMessage: String
}
