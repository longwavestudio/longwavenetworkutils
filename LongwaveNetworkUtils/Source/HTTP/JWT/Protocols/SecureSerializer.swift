//
//  SecureSerializer.swift
//  NetworkUtils
//
//  Created by Massimo Cesaraccio on 08/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public protocol SecureSerializer {
    func store(value: String, forKey: String)
    func load(_ key: String) -> String?
    func delete(_ key: String)
}
