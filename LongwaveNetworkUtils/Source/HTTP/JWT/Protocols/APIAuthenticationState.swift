//
//  APIAuthenticationState.swift
//  NetworkUtils
//
//  Created by Massimo Cesaraccio on 07/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

/// Describes the authentication state for the API layer.
public protocol APIAuthenticationState {
    /// The current access token, if any.
    var accessToken: String? { get }
    /// The current refresh token, if any.
    var refreshToken: String? { get }
    /// Returns _true_ when authenticated.
    func isAuthenticated() -> Bool
    /// Returns _true_ when an access token refresh is needed.
    func needsRefresh() -> Bool
    /// Will receive authentication and potentially transition state.
    /// - Parameters:
    ///   - accessToken: the new access token,
    ///   - refreshToken: the new refresh token,
    func authenticate(withAccessToken accessToken: String, refreshToken: String)
    /// Will remove all authentication info.
    func removeAuthentication()
    /// Will invalidate the current access token, after this function is called the state will require a token refresh.
    func setNeedsRefresh()
    /// Updates the access token.
    func refreshAccessToken(_ accessToken: String) throws
}
