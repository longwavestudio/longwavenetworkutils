//
//  APIAuthentication.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public protocol APIAuthentication: ExpirableAuthentication {
    var accessToken: String { get }
    var refreshToken: String { get }
}
