//
//  ExpirableAuthentication.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public protocol ExpirableAuthentication {
    /// Check if the associated instance is expired
    func isAuthenticationExpired() -> Bool
}

extension ExpirableAuthentication {
    func isAuthenticationExpired() -> Bool {
        return false
    }
}
