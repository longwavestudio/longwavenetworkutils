//
//  Authentication.swift
//  NetworkUtils
//
//  Created by Massimo Cesaraccio on 07/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation
import LongwaveUtils

/// An implementation of the API authentication state as a state machine.
public final class Authentication: APIAuthenticationState {
    public var accessToken: String? { state.accessToken }
    public var refreshToken: String? { state.refreshToken }
    
    private(set) public var serializer: SecureSerializer
    private var state: AuthenticationState = .unauthenticated {
        didSet {
            notifications.notify(state)
        }
    }
    private let accessTokenSerializationKey = "ApiAccessToken"
    private let refreshTokenSerializationKey = "ApiRefreshToken"
    
    public let notifications = Notifier<AuthenticationState>()
    
    public init(withSerializer serializer: SecureSerializer) {
        self.serializer = serializer
        if let accessToken = serializer.load(accessTokenSerializationKey),
            let refreshToken = serializer.load(refreshTokenSerializationKey) {
            state = .authenticated(access: accessToken, refresh: refreshToken)
        }
    }
    
    public func isAuthenticated() -> Bool {
        switch state {
        case .authenticated:
            return true
        default:
            return false
        }
    }
    
    public func needsRefresh() -> Bool {
        switch state {
        case .waitingRefresh:
            return true
        case .authenticated, .unauthenticated:
            return false
        }
    }
    
    public func authenticate(withAccessToken accessToken: String, refreshToken: String) {
        state = .authenticated(access: accessToken, refresh: refreshToken)
        serializer.store(value: accessToken, forKey: accessTokenSerializationKey)
        serializer.store(value: refreshToken, forKey: refreshTokenSerializationKey)
    }
    
    public func refreshAccessToken(_ accessToken: String) throws {
        switch state {
        case .authenticated(_, let refresh), .waitingRefresh(let refresh):
            state = .authenticated(access: accessToken, refresh: refresh)
        default:
            throw AuthenticationTokensError.forbiddenAuthenticationChange
        }
    }
    
    public func removeAuthentication() {
        state = .unauthenticated
        serializer.delete(accessTokenSerializationKey)
        serializer.delete(refreshTokenSerializationKey)
    }
    
    public func setNeedsRefresh() {
        switch state {
        case .authenticated(_, let refresh):
            state = .waitingRefresh(refresh: refresh)
        default:
            return
        }
    }
}

/// Models the internal state.
public enum AuthenticationState {
    case unauthenticated
    case authenticated(access: String, refresh: String)
    case waitingRefresh(refresh: String)
    
    var accessToken: String? {
        switch self {
        case .authenticated(let access, _):
            return access
        default:
            return nil
        }
    }
    
    var refreshToken: String? {
        switch self {
        case .authenticated(_, let refresh):
            return refresh
        case .waitingRefresh(let refresh):
            return refresh
        default:
            return nil
        }
    }
}

enum AuthenticationTokensError: Error {
    case forbiddenAuthenticationChange
}
