//
//  APIEndpoint.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public enum APIHttpMethod {
    case get
    case post
    case put
    case delete
    case head
}

public enum APIExecutionPermission {
    case notAuthenticated
    case authenticated
}

public enum APIConfigurationError: Error {
    case notConfigured
}

public typealias ParametersForIdentifier = (_ parameterId: String) -> String?

private let openParameter = "{"
private let closeParameter = "}"

public struct ApiError: Decodable, Error {
    public var code: Int
    public var message: String
    public var status: Int
}

public enum EndopointParametersType {
    case insideURL
    case query
}

/// Defines a generic HTTP API endpoint together with its own http method needed to be executed.
public protocol APIEndpoint {
    
    /// The apiIdentifier to be used to register the api in the manager
    var apiIdentifier: String { get }
    
    /// Returns the endPoint for a given API
    var endPoint: String { get }
    
    /// Starting parameter  separator. Default set to '{'.
    var openParameterSeparator: String { get }
    
    /// Closing parameter  separator. Default set to '}'.
    var closeParameterSeparator: String { get }
    
    /// Specifies the HttpMethod for the API. Default set to .get.
    var httpMethod: APIHttpMethod { get }
   
    /// Returns the type that defines how parameters should be add to the URL
    var parametersType: EndopointParametersType { get }
    
    /// Returns the ordered list of all the parameters contained inside the endpoint url
    var parametersKeys: [String]? { get }

    /// Defines if the api should be executed as Authenticated or not. Default set to .notAuthenticated.
    var apiPermission: APIExecutionPermission { get }
    
    /// Returns the complete url substituting all ocurrences of the parameter identifiers with the specified value
    func endPointWithParameters(_ parametersForKey: ParametersForIdentifier?) throws -> String
}

/// Default implementations
public extension APIEndpoint {
    var httpMethod: APIHttpMethod { .get }
    var openParameterSeparator: String { openParameter }
    var closeParameterSeparator: String { closeParameter }
    var apiPermission: APIExecutionPermission { .notAuthenticated }
    
    func endPointWithParameters(_ parameterForKey: ParametersForIdentifier?) throws -> String {
        guard let requiredParametersKeys = parametersKeys else {
            return endPoint
        }
        guard let parameterForKey = parameterForKey else {
            throw APIConfigurationError.notConfigured
        }
        var completeEndPoint = endPoint
        var keyCount = 0
        for key in requiredParametersKeys {
            switch parametersType {
            case .insideURL:
                guard let parameter = parameterForKey(key) else {
                    throw APIConfigurationError.notConfigured
                }
                let keyWithSeparators = openParameterSeparator + key + closeParameterSeparator
                completeEndPoint = completeEndPoint.replacingOccurrences(of: keyWithSeparators, with: parameter)
            case .query:
                if let parameter = parameterForKey(key)  {
                    let keyWithSeparators = openParameterSeparator + key + closeParameterSeparator
                    completeEndPoint = completeEndPoint.replacingOccurrences(of: keyWithSeparators, with: parameter)
                }
                let parameter = parameterForKey(key) ?? ""
                if keyCount == 0 {
                    completeEndPoint += "?\(key)=\(parameter)"
                } else {
                    completeEndPoint += "&\(key)=\(parameter)"
                }
                keyCount += 1
            }
        }
        return completeEndPoint
    }
    
    func endpoint(withParameters parameters: [String: Any], baseURL: String) throws -> String {
        guard let expectedParameters = parametersKeys else {
            return baseURL + endPoint
        }
        guard parametersType == .insideURL else {
            return try baseURL + endPointWithParameters { key in return parameters[key] as? String }
        }
        guard parameters.keys.count >= expectedParameters.count else {
            throw APIExecutionError.apiNotFound
        }
        for key in expectedParameters {
            if parameters[key] == nil {
                throw APIExecutionError.apiNotFound
            }
        }
        return try baseURL + endPointWithParameters { key in return parameters[key] as? String }
    }
}
