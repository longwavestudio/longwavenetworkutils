//
//  LoginService.swift
//  NetworkUtils
//
//  Created by Massimo Cesaraccio on 08/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

public typealias ServiceApiEngine = HttpAPIExecutor & HttpAPIManagerConfiguration

public protocol LoginResponse: Decodable {
    func tokens() -> (accessToken: String, refreshToken: String)?
}

/// The service in charge of authentication operations.
public final class LoginService {
    let engine: ServiceApiEngine
    private let serializer: SecureSerializer
    private let authentication: APIAuthenticationState
    private let deviceIdSerializationKey = "UserDeviceId"

    private var refreshCompletionHandlers: [Any] = []
    
    /// Creates a new instance, given an api engine.
    /// - Parameter engine: The api engine the service will use.
    public init(engine: ServiceApiEngine, serializer: SecureSerializer, authentication: APIAuthenticationState) {
        self.engine = engine
        self.serializer = serializer
        self.authentication = authentication
        registerApis()
    }
    
    private func registerApis() {
        generateApis().forEach { api in
            engine.registerApi(api: api) { (apiId, ok) in
                if (!ok) {
                    print("Login Service - failed registering api: \(apiId)")
                }
            }
        }
    }
    
    private func generateApis() -> [APIEndpoint] {
        return [LoginAPI(), LogoutAPI(), RefreshTokenAPI()]
    }
    
    public func login<T: LoginResponse>(username: String,
                                        password: String,
                                        completion: @escaping (Result<T, Error>) -> Void) {
        let deviceId = deserializeDeviceId() ?? UUID().uuidString
        let parameters = [ApiParameters.Login.username.rawValue: username.trimmingCharacters(in: .newlines),
                          ApiParameters.Login.password.rawValue: password.trimmingCharacters(in: .newlines),
                          ApiParameters.Login.deviceId.rawValue: deviceId]
        engine.executeApi(identifier: ApiMap.login.rawValue,
                          endpointParameters: [:],
                          headers: [],
                          parameters: parameters) { [weak self] result in
                            self?.handleLoginResponse(result, deviceId: deviceId, completion: completion)
        }
    }
    
    public func logout(completion: ((Result<VoidResponse, Error>) -> Void)? = nil) {
        guard let deviceId = deserializeDeviceId() else {
            completion?(.failure(LoginError.invalidOperation))
            return
        }
        let parameters = [ApiParameters.Logout.deviceId.rawValue: deviceId]
        engine.executeApi(identifier: ApiMap.logout.rawValue,
                          endpointParameters: [:],
                          headers: [],
                          parameters: parameters) { [weak self] (res: Result<VoidResponse, Error>) in
                            self?.authentication.removeAuthentication()
                            completion?(res)
        }
    }
    
    private func deserializeDeviceId() -> String? {
        return serializer.load(deviceIdSerializationKey)
    }
}

extension LoginService: TokenRefreshService {
    public func refreshToken<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void) {
        guard !authentication.needsRefresh() else {
            // we are already refreshing
            refreshCompletionHandlers.append(completion)
            return
        }
        guard let refreshToken = authentication.refreshToken else {
            completion(.failure(LoginError.invalidRefreshToken))
            return
        }
        let parameters = [ApiParameters.RefreshToken.refreshToken.rawValue: refreshToken]
        authentication.setNeedsRefresh()
        engine.executeApi(identifier: ApiMap.refreshToken.rawValue,
                          endpointParameters: [:],
                          headers: [],
                          parameters: parameters) { [weak self] result in
                            self?.handleTokenRefreshResponse(result, completion: completion)
        }
    }
    
    public func accessToken() -> String? {
        return authentication.accessToken
    }
}

extension LoginService {
    private func handleTokenRefreshResponse<T: LoginResponse>(_ response: Result<T, Error>,
                                                              deviceId: String? = nil,
                                                              completion: @escaping (Result<T, Error>) -> Void) {
        do {
            try updateAuthenticationFromResponse(response)
            completion(response)
            if let pendingHandlers = refreshCompletionHandlers as? [(Result<T, Error>) -> Void] {
                for handler in pendingHandlers {
                    handler(response)
                }
                refreshCompletionHandlers = []
            }
        }
        catch {
            authentication.removeAuthentication()
            completion(.failure(error))
        }
    }
    private func handleLoginResponse<T: LoginResponse>(_ response: Result<T, Error>,
                                                       deviceId: String? = nil,
                                                       completion: @escaping (Result<T, Error>) -> Void) {
        do {
            try updateAuthenticationFromResponse(response)
            saveDeviceId(deviceId)
            completion(response)
        }
        catch {
            completion(.failure(error))
        }
    }
    
    private func updateAuthenticationFromResponse<T: LoginResponse>(_ response: Result<T, Error>) throws {
        let data = try response.get()
        guard let tokens = data.tokens() else { throw LoginError.invalidAuthenticationData }
        authentication.authenticate(withAccessToken: tokens.accessToken, refreshToken: tokens.refreshToken)
    }
    
    private func saveDeviceId(_ deviceId: String?) {
        if let deviceId = deviceId {
            serializer.store(value: deviceId, forKey: self.deviceIdSerializationKey)
        }
    }
}

private struct LoginAPI: APIEndpoint {
    let apiIdentifier = ApiMap.login.rawValue
    let endPoint: String = "/login"
    let httpMethod: APIHttpMethod = .post
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = nil
    let apiPermission: APIExecutionPermission = .notAuthenticated
}

private struct RefreshTokenAPI: APIEndpoint {
    let apiIdentifier = ApiMap.refreshToken.rawValue
    let endPoint: String = "/refresh"
    let httpMethod: APIHttpMethod = .post
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = nil
    let apiPermission: APIExecutionPermission = .notAuthenticated
}

private struct LogoutAPI: APIEndpoint {
    let apiIdentifier = ApiMap.logout.rawValue
    let endPoint: String = "/user/logout"
    let httpMethod: APIHttpMethod = .delete
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = nil
    let apiPermission: APIExecutionPermission = .authenticated
}

private enum ApiMap: String {
    case login = "UserLogin"
    case refreshToken = "RefreshToken"
    case logout = "UserLogout"
}

fileprivate struct ApiParameters {
    fileprivate enum Login: String {
        case username
        case password
        case deviceId
    }
    fileprivate enum RefreshToken: String {
        case refreshToken
    }
    fileprivate enum Logout: String {
        case deviceId
    }
}

enum LoginError: Error {
    case invalidRefreshToken
    case invalidAuthenticationData
    case invalidOperation
}
