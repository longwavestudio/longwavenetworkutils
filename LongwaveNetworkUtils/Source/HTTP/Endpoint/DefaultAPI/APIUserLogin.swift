//
//  APIUserLogin.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 20/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

final class APIUserLogin: APIEndpoint {
    
    let apiIdentifier = "POST_userlogin"
    let endPoint: String = "/user/login"
    let httpMethod: APIHttpMethod = .post
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = nil
    let apiPermission: APIExecutionPermission = .notAuthenticated
    private init() {}
    
    public static func defaultUserLogin() -> APIUserLogin {
        return APIUserLogin()
    }
}
