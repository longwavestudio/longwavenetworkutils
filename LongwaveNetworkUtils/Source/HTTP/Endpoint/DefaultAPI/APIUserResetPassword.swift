//
//  APIUserResetPassword.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 20/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

final class APIUserChangePassword: APIEndpoint {
    
    let apiIdentifier = "POST_userchangepassword"
    let endPoint: String = "/user/{email}/changepassword"
    let httpMethod: APIHttpMethod = .post
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = ["email"]
    let apiPermission: APIExecutionPermission = .notAuthenticated
    private init() {}
    
    public static func defaultUserChangePassword() -> APIUserChangePassword {
        return APIUserChangePassword()
    }
}

