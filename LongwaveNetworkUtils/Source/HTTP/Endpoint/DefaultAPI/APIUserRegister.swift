//
//  APIUserRegister.swift
//  NetworkUtils
//
//  Created by Alessio Bonu on 20/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation

final class APIUserRegister: APIEndpoint {
    
    let apiIdentifier = "POST_userregister"
    let endPoint: String = "/user/register"
    let httpMethod: APIHttpMethod = .post
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = nil
    let apiPermission: APIExecutionPermission = .notAuthenticated
    private init() {}
    
    public static func defaultUserRegister() -> APIUserRegister {
        return APIUserRegister()
    }
}
