# LongwaveNetworkUtils

[![CI Status](https://img.shields.io/travis/mcesaraccio@gmail.com/LongwaveNetworkUtils.svg?style=flat)](https://travis-ci.org/mcesaraccio@gmail.com/LongwaveNetworkUtils)
[![Version](https://img.shields.io/cocoapods/v/LongwaveNetworkUtils.svg?style=flat)](https://cocoapods.org/pods/LongwaveNetworkUtils)
[![License](https://img.shields.io/cocoapods/l/LongwaveNetworkUtils.svg?style=flat)](https://cocoapods.org/pods/LongwaveNetworkUtils)
[![Platform](https://img.shields.io/cocoapods/p/LongwaveNetworkUtils.svg?style=flat)](https://cocoapods.org/pods/LongwaveNetworkUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LongwaveNetworkUtils is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LongwaveNetworkUtils'
```

## Author

mcesaraccio@gmail.com, mcesaraccio@gmail.com

## License

LongwaveNetworkUtils is available under the MIT license. See the LICENSE file for more info.
