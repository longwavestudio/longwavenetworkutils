//
//  ViewController.swift
//  LongwaveNetworkUtils
//
//  Created by mcesaraccio@gmail.com on 04/15/2020.
//  Copyright (c) 2020 mcesaraccio@gmail.com. All rights reserved.
//

import UIKit
import LongwaveNetworkUtils
import Alamofire
import Combine

struct GenericAPIConfiguration: APIConfiguration {
    var baseURL = URL(string: "https://api.intendime.com")!
}

private enum ApiMap: String {
    case configuration
}

private struct ConfigurationApi: APIEndpoint {
    let apiIdentifier = ApiMap.configuration.rawValue
    let endPoint: String = "/config"
    let httpMethod: APIHttpMethod = .get
    var parametersType: EndopointParametersType = .insideURL
    let parametersKeys: [String]? = []
    let apiPermission: APIExecutionPermission = .notAuthenticated
}

class ViewController: UIViewController {

    let handler = DefaultNetworkHandler(session: AF)
    var engine: HttpAPIManager!
    
    var cancellableRequest: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        engine = HttpAPIManager(queue: DispatchQueue(label: "example"),
//                                requestProvider: handler,
//                                configuration: GenericAPIConfiguration())
//        engine.registerApi(api: ConfigurationApi(), onRegistered: nil)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if #available(iOS 13.0, *) {
//                do {
//                    let request: Future<HelloConfiguration, Error>?
//                    request = try self.engine.executeApi(identifier: ApiMap.configuration.rawValue,
//                                                     endpointParameters: [:],
//                                                     headers: [],
//                                                     parameters: [:])
//                    self.cancellableRequest = request?.sink { error in
//                        print("Error \(error)")
//                    } receiveValue: { configuration in
//                        print("Completed \(configuration)")
//                    }
//                } catch {
//                    print("Request not created \(error)")
//                }
//            } else {
//                // Fallback on earlier versions
//            }
//
//        }
        
    }

}

