//
//  NetworkHandler.swift
//  Longwave BaseProject
//
//  Created by Massimo Cesaraccio on 28/05/2020.
//  Copyright © 2020 Longwave Studio S.r.l. All rights reserved.
//

import Foundation
import Alamofire
import LongwaveNetworkUtils
import Combine

enum NetworkError: Error {
    case genericError
    case configurationNetworkError
    case wrongResponseType
    case invalidUrl
    case unauthenticated
}

struct HelloConfiguration: Decodable {
    var baseURL: String?
}

extension APIHttpMethod {
    func alamofireHttpMethod() -> HTTPMethod {
        switch self {
        case .delete:
            return .delete
        case .get:
            return .get
        case .head:
            return .head
        case .post:
            return .post
        case .put:
            return .put
        }
    }
}

protocol ConfigurationProvider {
    
}

extension APIHttpHeader {
    func alamofireHttpHeader() -> HTTPHeader {
        return HTTPHeader(name: name, value: value)
    }
}

let helloAppId = "hellomajvsjnpcmn"

enum HelloHeaderFields: String {
    case authorization = "Authorization"
    case appId = "QUISQUE-APP-ID"
    case sessionId = "QUISQUE-SESSION-ID"
    case access = "X-QUISQUE-ACCESS"
}

enum ConfigurationData: String {
    case appBundle
    case appName
    case appVersion
    case enelXSdkVersion
    case os
    case osVersion
    case qBoxSdkVersion
}

struct LoginData: LoginResponse {
    func tokens() -> (accessToken: String, refreshToken: String)? {
        return nil
    }
}

final class DefaultTokenRefreshService: TokenRefreshService {
    func refreshToken<T>(completion: @escaping (Result<T, Error>) -> Void) where T : LoginResponse {
        completion(.success(LoginData() as! T))
    }
    
    func accessToken() -> String? {
        return nil
    }
}

final class DefaultNetworkHandler {
    var tokenRefreshService: TokenRefreshService?
    private(set) var session: Session
    var notificationCenter = NotificationCenter.default
    private(set) var apiSessionId: String?

    private var configurationUrlStr = "https://dev-api.quisque.io/v2/configurations"
    init(session: Session = AF) {
        self.session = session
        tokenRefreshService = DefaultTokenRefreshService()
    }
}

let logoutRequiredNotificationName = Notification.Name("logoutRequiredNotificationName")
final class LocalInterceptor: RequestInterceptor {
    private(set) var api: APIEndpoint
    private(set) var refreshService: TokenRefreshService
    private var notificationCenter: NotificationCenter
    private(set) var apiSessionId: String

    init(api: APIEndpoint,
         apiSessionId: String,
         refreshService: TokenRefreshService,
         notificationCenter: NotificationCenter) {
        self.api = api
        self.refreshService = refreshService
        self.apiSessionId = apiSessionId
        self.notificationCenter = notificationCenter
    }
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var urlRequest = urlRequest
        urlRequest.setValue(helloAppId,
                            forHTTPHeaderField: HelloHeaderFields.appId.rawValue)
        urlRequest.setValue(apiSessionId,
                            forHTTPHeaderField: HelloHeaderFields.sessionId.rawValue)
        urlRequest.setValue("USER",
                            forHTTPHeaderField: HelloHeaderFields.access.rawValue)
        guard let token = refreshService.accessToken() else {
            guard api.apiPermission == .notAuthenticated else {
                completion(.failure(NetworkError.unauthenticated))
                return
            }
            completion(.success(urlRequest))
            return
        }
        urlRequest.setValue("Bearer \(token)",
                            forHTTPHeaderField: HelloHeaderFields.authorization.rawValue)
        completion(.success(urlRequest))
    }
    
    private func shouldRetryOnError(_ code: Int) -> Bool {
        return code == 401 && api.apiPermission == .authenticated
    }       
}

extension DefaultNetworkHandler: ConfigurationProvider {}
extension DefaultNetworkHandler: HTTPRequestProvider {
    
    @available(iOS 13.0, *)
    func request<ResponseType>(_ api: APIEndpoint, baseURL: URL, headers: [APIHttpHeader]?, endpointParameters: [String : String], parameters: [String : Any]?) -> Future<ResponseType, Error> where ResponseType : Decodable {
        return request(api,
                       baseURL: baseURL,
                       parameters: parameters,
                       headers: headers,
                       endpointParameters: endpointParameters)
    }
    
    @available(iOS 13.0, *)
    private func request<T: Decodable, E: Error>(_ api: APIEndpoint,
                                         baseURL: URL,
                                         parameters: Parameters?,
                                         decoder: JSONDecoder = JSONDecoder(),
                                         headers: [APIHttpHeader]?,
                                         endpointParameters: [String: String]) -> Future<T, E> {
        let alamofireHeaders = alamofireHTTPHeaders(from: headers)
        return Future({ [weak self] promise in
            guard let self = self,
                  let apiUrl = try? api.endpoint(withParameters: endpointParameters,
                                                 baseURL: baseURL.absoluteString) else {
                promise(.failure(NetworkError.invalidUrl as! E))
                return
            }
            let interceptorForRequest = self.interceptor(forApi: api)
            AF.request(
                apiUrl,
                method: api.httpMethod.alamofireHttpMethod(),
                parameters: parameters,
                headers: alamofireHeaders,
                interceptor: interceptorForRequest).responseDecodable(decoder: decoder, completionHandler: { (response: DataResponse<T, AFError>) in
                    switch response.result {
                    case .success(let value):
                        promise(.success(value))
                    case .failure(let error):
                        let error = NSError(domain: error.destinationURL?.absoluteString ?? "",
                                            code: error.responseCode ?? 0) as! E
                        promise(.failure(error))
                    }
                }).validate(statusCode: 200..<300)
        })
    }
    
    func retrieveConfiguration(completion: @escaping OnConfigurationDidRetrieve) {
        let headers = HTTPHeaders([HTTPHeader(name: HelloHeaderFields.appId.rawValue,
                                              value:  helloAppId)])
        let parameters: [String: String] = [ConfigurationData.appBundle.rawValue: "quisque.io.ready2park",
                                            ConfigurationData.appName.rawValue: "ready to park",
                                            ConfigurationData.appVersion.rawValue: "1.9.3",
                                            ConfigurationData.enelXSdkVersion.rawValue:  "1.0.0",
                                            ConfigurationData.os.rawValue: "android",
                                            ConfigurationData.osVersion.rawValue: "14",
                                            ConfigurationData.qBoxSdkVersion.rawValue: "2.0.0"]
        session.request(configurationUrlStr,
                        method: .post,
                        parameters: parameters,
                        encoder: JSONParameterEncoder(),
                        headers: headers).response {[weak self] responce in
                            switch responce.result {
                            case .success(let data):
                                guard let data = data else {
                                    DispatchQueue.main.async {
                                        completion(.failure(NetworkError.configurationNetworkError))
                                    }
                                    return
                                }
                                DispatchQueue.main.async {
                                    guard let self = self else { return }
                                    self.apiSessionId = self.extraxtSessionId(data)
                                    let newData = self.adaptConfigurationForStartupHandler(data)
                                    completion(.success(newData))
                                }
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    completion(.failure(error))
                                }
                            }
                        }
    }
    
    private func extraxtSessionId(_ data: Data) -> String {
        _ = try? JSONDecoder().decode(HelloConfiguration.self,
                                                      from: data)
        return ""
    }
    
    private func adaptConfigurationForStartupHandler(_ data: Data) -> Data {
        return data
    }
    
    func request<ResponseType>(_ api: APIEndpoint,
                               baseURL: URL,
                               headers: [APIHttpHeader]?,
                               endpointParameters: [String: String],
                               parameters: [String: Any]?,
                               completion: @escaping OnRequestDidComplete<ResponseType>) where ResponseType: Decodable {
        let alamofireHeaders = alamofireHTTPHeaders(from: headers)
        guard let apiUrl = try? api.endpoint(withParameters: endpointParameters,
                                             baseURL: baseURL.absoluteString) else {
            completion(.failure(NetworkError.invalidUrl))
            return
        }
        let interceptorForRequest = interceptor(forApi: api)
        session.request(apiUrl,
                        method: api.httpMethod.alamofireHttpMethod(),
                        parameters: parameters,
                        encoding: alamofireEncodingForMethod(api.httpMethod),
                        headers: alamofireHeaders,
                        interceptor: interceptorForRequest)
            .validate(statusCode: 200..<300)
            .response { response in
                guard response.error == nil else {
                    
                    DispatchQueue.main.async {
                        completion(.failure( NetworkError.genericError))
                    }
                    return
                }
                // To allow handling empty responses we generate an empty json to allow JSONDecoder to parse, for allowed types
                print("json: \(String(describing: String(data: response.data ?? Data(), encoding: .utf8)))")
                self.handleResponceData(response.data, completion: completion)
            }
    }
    
    private func handleResponceData<ResponseType>(_ data: Data?, completion: @escaping OnRequestDidComplete<ResponseType>) {
        guard let data = data ?? DefaultNetworkHandler.voidResponseData(for: ResponseType.self) else {
            DispatchQueue.main.async {
                completion(.failure(NetworkError.wrongResponseType))
            }
            return
        }
        do {
            let responseDecodedData = try JSONDecoder().decode(ResponseType.self, from: data)
            DispatchQueue.main.async {
                completion(.success(responseDecodedData))
            }
        } catch {
            print("parsing error \(error)")
            DispatchQueue.main.async {
                completion(.failure(NetworkError.wrongResponseType))
            }
        }
    }
    
    
    private static func voidResponseData<ResponseType>(for type: ResponseType.Type) -> Data? where ResponseType: Decodable {
        guard type is VoidResponse.Type else { return nil }
        return "{}".data(using: .utf8)!
    }
    
    private func interceptor(forApi api: APIEndpoint) -> RequestInterceptor? {
        guard let service = tokenRefreshService else {
            return nil
        }
        return LocalInterceptor(api: api, apiSessionId: "", refreshService: service, notificationCenter: notificationCenter)
    }
    
    private func alamofireEncodingForMethod(_ method: APIHttpMethod) -> ParameterEncoding {
        switch method {
        case .post, .put:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    private func alamofireHTTPHeaders(from headers: [APIHttpHeader]?) -> HTTPHeaders? {
        guard let headers = headers else {
            return nil
        }
        let afHeaders = headers.map({ $0.alamofireHttpHeader() })
        return HTTPHeaders(afHeaders)
    }
}


