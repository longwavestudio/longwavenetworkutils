//
//  HttpAPIManagerTests.swift
//  NetworkUtilsTests
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import XCTest
@testable import LongwaveNetworkUtils

class HttpAPIManagerTests: XCTestCase {

    let configurationURLMock = APIConfigurationData(baseURL: URL(string: "http://www.longwave.studio")!)

    func testRegisterFirstAPIShouldSucceed() {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock )
        var apiRegisteredBlockCalled = false
        sut.registerApi(api: GetAPIWithNoParameter()) { identifier, isRegistered in
            apiRegisteredBlockCalled = true
            XCTAssertTrue(isRegistered)
            XCTAssertEqual(identifier, identifier)
        }
        XCTAssertTrue(apiRegisteredBlockCalled)
    }
    
    func testRegisterAnotherAPIShouldSucceed() {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        var apiRegisteredFirstBlockCalled = false
        var apiRegisteredSecondBlockCalled = false
        let api1 = GetAPIWithNoParameter()
        api1.apiIdentifier = "aaa"
        let api2 = GetAPIWithNoParameter()
        api2.apiIdentifier = "bbb"
        sut.registerApi(api: api1) { identifier, isRegistered in
            apiRegisteredFirstBlockCalled = true
            XCTAssertTrue(isRegistered)
            XCTAssertEqual(identifier, "aaa")
        }
        sut.registerApi(api: api2) { identifier, isRegistered in
            apiRegisteredSecondBlockCalled = true
            XCTAssertTrue(isRegistered)
            XCTAssertEqual(identifier, "bbb")
        }
        XCTAssertTrue(apiRegisteredFirstBlockCalled)
        XCTAssertTrue(apiRegisteredSecondBlockCalled)
    }
    
    func testRegisterTwoAPIsWithTheSameIdentifierShouldSucceedForTheFirstAndFailForTheSecond() {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        let apiIdentifier1 = "aaa"
        var apiRegisteredFirstBlockCalled = false
        var apiRegisteredSecondBlockCalled = false
        
        sut.registerApi(api: GetAPIWithNoParameter()) { identifier, isRegistered in
            apiRegisteredFirstBlockCalled = true
            XCTAssertTrue(isRegistered)
            XCTAssertEqual(identifier, apiIdentifier1)
        }
        sut.registerApi(api: GetAPIWithNoParameter()) { identifier, isRegistered in
            apiRegisteredSecondBlockCalled = true
            XCTAssertFalse(isRegistered)
            XCTAssertEqual(identifier, apiIdentifier1)
        }
        XCTAssertTrue(apiRegisteredFirstBlockCalled)
        XCTAssertTrue(apiRegisteredSecondBlockCalled)
    }
    
    func testRegisterMultipleAPIsShouldSucceed() {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        let apiIdentifier1 = "aaa"
        let apiIdentifier2 = "bbb"

        var apiRegisteredBlockCalled = false

        if #available(iOS 13.0, *) {
            sut.registerApis( [apiIdentifier1: GetAPIWithNoParameter(),
                               apiIdentifier2: GetAPIWithNoParameter()]) {
                apiRegisteredBlockCalled = true
            }
            XCTAssertTrue(apiRegisteredBlockCalled)
        } else {
            let api1 = GetAPIWithNoParameter()
            api1.apiIdentifier = apiIdentifier1
            sut.registerApi(api: api1, onRegistered: nil)
            let api2 = GetAPIWithNoParameter()
            api2.apiIdentifier = apiIdentifier2
            sut.registerApi(api: api2, onRegistered: nil)
        }
    }
    
    func testExecutingApiWithoutHavingCalledConfigurationRetrievingShouldRaiseAPIConfigurationNotReady() {
        let mock = RequestProviderMock()
        let manager = parametersTests(parameters: [:],
                        registeredAPIs: ["aaa" : GetAPIWithNoParameter()],
                        networkMock: mock,
                        shouldLoadConfiguration: false) { error, _ in
                            if error != APIExecutionError.configurationNotReady {
                                XCTFail("Should be APIExecutionError.configurationNotReady")
                            }
        }
        XCTAssertNotNil(manager)
    }
}


/// HttpAPIExecutor
extension HttpAPIManagerTests {
    func testAPIManagerIfAPIIsNotConfiguredShouldCompleteWithError() {
        parametersTests(parameters: [:],
                        registeredAPIs: [:]) {  error, result in
                            if error != APIExecutionError.apiNotFound {
                                XCTFail("Should be APIExecutionError.apiNotFound")
                            }
        }
    }
    
    func testIfAllParametersHaveBeenFoundTheExecutionShouldCompleteSuccessfully() {
        parametersTests(parameters: ["typeId": "type"],
                        registeredAPIs: ["aaa" : GetAPIWithParameter()]) { error, result in
                            
        }
    }

}

extension HttpAPIManagerTests {
    func testSchedulingMethodShouldBeExecutedSynchronouslyWithNoQueueSpecified() {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        var calledBlock = false
        sut.schedule {
            calledBlock = true
        }
        XCTAssertTrue(calledBlock)
    }
    
    func testExecutingMethodShouldBeExecutedSynchronouslyWithNoQueueSpecified() {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        var calledBlock = false
        sut.execute {
            calledBlock = true
        }
        XCTAssertTrue(calledBlock)
    }
    
    func testSchedulingMethodShouldBeExecutedAsynchronouslyWithDefaultQueue() {
        let sut = HttpAPIManager(requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        let exp = expectation(description: "testSchedulingMethodShouldBeExecutedAsynchronouslyWithDefaultQueue")
        sut.schedule {
            exp.fulfill()
        }
        waitForExpectations(timeout: 0.1)
    }
    
    func testExecutingMethodShouldBeExecutedSynchronouslyWithDefaultQueue() {
        let sut = HttpAPIManager(requestProvider: RequestProviderMock(),
                                 configuration: configurationURLMock)
        var calledBlock = false
        sut.execute {
            calledBlock = true
        }
        XCTAssertTrue(calledBlock)
    }
}
/// Authentication tests
extension HttpAPIManagerTests {
        
    struct TestAuthenticationToken: APIAuthentication {
        var accessToken: String = "token"
        var refreshToken: String = "refreshToken"
        var isExpired = true
        func isAuthenticationExpired() -> Bool {
            return isExpired
        }
    }
}

/// Response validation
extension HttpAPIManagerTests {

    func testValidationWithManagerDeallocatedShouldReturnSuccess() {
        let result = validateStatusCode(300,
                                        discardManager: true)
        switch result {
        case .success(_):
            break
        case .failure(_):
            XCTFail("Should succeed")
        }
    }
    
    func testValidationWithStatusCodeBetween200and299ShouldSucceedWith200() {
        let result = validateStatusCode(200)
        switch result {
        case .success(_):
            break
        case .failure(_):
            XCTFail("Should succeed")
        }
    }
    
    func testValidationWithStatusCodeBetween200and299ShouldSucceedWith299() {
        let result = validateStatusCode(299)
        switch result {
        case .success(_):
            break
        case .failure(_):
            XCTFail("Should succeed")
        }
    }
}

///ExpirableAuthentication
extension HttpAPIManagerTests {
    func test_httpHeadere_shouldBeCreated() {
        let header = APIHttpHeader(name: "aa", value: "bbb")
        XCTAssertEqual(header.name, "aa" )
        XCTAssertEqual(header.value, "bbb" )
    }
}

///ExpirableAuthentication
extension HttpAPIManagerTests {
    func testExpirableAuthenticationByDefaultIsExpiredIsSetToFalse() {
        struct TestExpiration: ExpirableAuthentication {}
        let sut = TestExpiration()
        XCTAssertFalse(sut.isAuthenticationExpired())
    }
}

/// Private methods
extension HttpAPIManagerTests {
    @discardableResult
    private func parametersTests(parameters: [String: String],
                                 registeredAPIs: [String: APIEndpoint],
                                 networkMock: RequestProviderMock = RequestProviderMock(),
                                 shouldLoadConfiguration: Bool = true,
                                 completion: @escaping (APIExecutionError?, String?) -> Void) -> HttpAPIManager {
        let sut = HttpAPIManager(queue: nil,
                                 requestProvider: networkMock,
                                 configuration: shouldLoadConfiguration ? configurationURLMock : nil)
        var isAPICompletionCalled = false
        let identifier = "aaa"
        sut.retrieveConfiguration { _ in }
        if #available(iOS 13.0, *) {
            sut.registerApis(registeredAPIs, onRegistered: nil)
        } else {
            for (key, api) in registeredAPIs {
                let current = api
                (current as? GetAPIWithNoParameter)?.apiIdentifier  = key
                (current as? AuthenticatedGetAPIWithNoParameters)?.apiIdentifier  = key
                (current as? GetAPIWithParameter)?.apiIdentifier  = key

                
                sut.registerApi(api: current, onRegistered: nil)
            }
        }
        sut.executeApi(identifier: identifier,
                       endpointParameters: parameters,
                       headers: [],
                       parameters: [:],
                       onCompletion: { result in
                        isAPICompletionCalled = true
                        switch result {
                        case .failure(let error):
                            completion(error as? APIExecutionError, nil)
                        case .success(let expectedString):
                            completion(nil, expectedString)
                        }
                        } as (Result<String, Error>) -> Void)
        let result: Result<String, Error> = .success("aaa")
        networkMock.response { result }
        XCTAssertTrue(isAPICompletionCalled)
        return sut
    }
    
    private func validateStatusCode(_ statusCode: Int,
                                    data: Data? = nil,
                                    discardManager: Bool = false) -> Result<Void, Error> {
        let mock = RequestProviderMock()
        let expectedParameter = "--type--"
        var manager: HttpAPIManager? = parametersTests(parameters: ["typeId": expectedParameter],
                                                       registeredAPIs: ["aaa" : GetAPIWithParameter()],
                                                       networkMock: mock) { _, _ in }
        if discardManager {
            manager = nil
        } else {
            XCTAssertNotNil(manager)
        }
        
        return .success(())
    }
}

struct DecodableStruct: Decodable {}
