//
//  RequestProviderMock.swift
//  NetworkUtilsTests
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation
@testable import LongwaveNetworkUtils
import Combine

enum TestError: Error {
    case generic
}

typealias OnRequestDidCall = (_ endpoint: String, _ method: APIHttpMethod, _ headers: [APIHttpHeader]?, _ parameters: [String: Any]?) -> Void

final class RequestProviderMock {
    var response: Any?
    var completion: Any?
    var configuration: APIConfigurationData?
    var returnCorruptedConfigurationData = false
    var onRequestCalled: OnRequestDidCall?
    
    var combineCallback: ((Result<Decodable, Error>) -> Void)? = { result in
        
    }
    
    func response<ResponseType: Decodable>(onResponse: @escaping () -> Result<ResponseType, Error>) {
        (completion as? (Result<ResponseType, Error>) -> Void)?(onResponse())
    }
    
}

extension RequestProviderMock: HTTPRequestProvider {
    @available(iOS 13, *)
    func request<ResponseType: Decodable>(_ api: APIEndpoint,
                               baseURL: URL,
                               headers: [APIHttpHeader]?,
                               endpointParameters: [String : String],
                               parameters: [String : Any]?) -> Future<ResponseType, Error> {
        return Future<ResponseType, Error>({ [weak self] completion in
            self?.combineCallback = completion as? (Result<Decodable, Error>) -> Void
        })
    }

    
    func retrieveConfiguration(completion: @escaping OnConfigurationDidRetrieve) {
        guard !returnCorruptedConfigurationData else {
            completion(.success(Data()))
            return
        }
        guard let configuration = self.configuration, let configurationData = try? JSONEncoder().encode(configuration) else {
            completion(.failure(TestError.generic))
            return
        }
        completion(.success(configurationData))
    }
    
    func request<ResponseType>(_ api: APIEndpoint, baseURL: URL, headers: [APIHttpHeader]?, endpointParameters: [String : String], parameters: [String : Any]?, completion: @escaping OnRequestDidComplete<ResponseType>) where ResponseType : Decodable {
        onRequestCalled?(api.endPoint, api.httpMethod, headers, parameters)
        self.completion = completion
    }
}
