//
//  ApiEndpointTests.swift
//  NetworkUtilsTests
//
//  Created by Alessio Bonu on 18/02/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import XCTest
@testable import LongwaveNetworkUtils

class ApiEndpointTests: XCTestCase {

    func testEndpointShouldBeValid() {
        let sut = GetAPIWithNoParameter()
        XCTAssertEqual(sut.endPoint, "/pippo/getName")
    }
    
    func testDefaultApiShouldHaveGetMethod() {
        let sut = DefautAPIWithNoParameter()
        XCTAssertEqual(sut.httpMethod, .get)
    }
    
    func testDefaultApiShouldHaveGetNotAuthenticatedPermission() {
        let sut = DefautAPIWithNoParameter()
        XCTAssertEqual(sut.apiPermission, .notAuthenticated)
    }
    
    func testHttpMethodShouldBeValid() {
        let sut = GetAPIWithNoParameter()
        XCTAssertEqual(sut.httpMethod, .post)
    }
    
    func testConfigureApiWithNoParameterInsideTheEndpointShouldReturnRawEndpoint() {
        let sut = GetAPIWithNoParameter()
        XCTAssertEqual(try? sut.endPointWithParameters(nil), "/pippo/getName")
    }

    func testConfigureApiWithParametersInsideTheEndpointShouldThrowIfNoClosureIsSet() {
        let sut = GetAPIWithParameter()
        XCTAssertThrowsError(try sut.endPointWithParameters(nil))
    }
    
    func testConfigureApiWithParametersInsideTheEndpointShouldNotThrowIfClosureIsSet() {
        let sut = GetAPIWithParameter()
        XCTAssertNoThrow(try sut.endPointWithParameters({ key in
            return "aaa"
        }))
    }
    
    func testConfigureApiWithParametersInsideTheEndpointShouldSubstituteParameter() {
        let sut = GetAPIWithParameter()
        let completeEndpoint = try? sut.endPointWithParameters({ key in
            return "aaa"
        })
        XCTAssertEqual(completeEndpoint, "/type/aaa")
    }
    
    func test_configureQueryApi_withParameters_shouldSubstituteParameter() {
        let sut = GetAPIWithQueryParameter()
        let completeEndpoint = try? sut.endPointWithParameters({ key in
            switch key {
            case "typeId":
                return "a"
            case "aaa":
                return "b"
            default:
                return ""
            }
        })
        XCTAssertEqual(completeEndpoint, "/type?typeId=a&aaa=b")
    }
    
    func test_configureQueryApi_withOneParameters_shouldSubstituteParameter() {
        let sut = GetAPIWithQueryOneParameter()
        let completeEndpoint = try? sut.endPointWithParameters({ key in
            switch key {
            case "typeId":
                return "a"
            default:
                return ""
            }
        })
        XCTAssertEqual(completeEndpoint, "/type?typeId=a")
    }
    
    func testConfigureApiWithParametersInsideTheEndpointShouldSubstituteMultipleParameter() {
        let sut = GetAPIWithMultupleParameters()
        let completeEndpoint = try? sut.endPointWithParameters({ key in
            switch key {
            case sut.parametersKeys![0]:
                return "111"
            case sut.parametersKeys![1]:
                return "222"
            default:
                return "---"
            }
        })
        XCTAssertEqual(completeEndpoint, "/type/111/subtype/222/")
    }
    
    func testConfigureApiWithLessParametersInsideTheEndpointShouldFail() {
        let sut = GetAPIWithMultupleParameters()
        XCTAssertThrowsError(try sut.endPointWithParameters({ key in
            switch key {
            case sut.parametersKeys![0]:
                return "111"
            default:
                return nil
            }
        }))
    }
}

/// DefaultEndpoints tests
extension ApiEndpointTests {
    
    func testDefaultLogin() {
        defaultAPIEndpointTest(sut: APIUserLogin.defaultUserLogin(),
                               expectedId: "POST_userlogin",
                               expectedEndPoint: "/user/login",
                               expectedMethod: .post,
                               expectedPermission: .notAuthenticated,
                               expectedParametersKey: nil,
                               expectedOpenParameterSeparator: "{",
                               expectedCloseParameterSeparator: "}")
    }
    
    func testDefaultRegistration() {
        defaultAPIEndpointTest(sut: APIUserRegister.defaultUserRegister(),
                               expectedId: "POST_userregister",
                               expectedEndPoint: "/user/register",
                               expectedMethod: .post,
                               expectedPermission: .notAuthenticated,
                               expectedParametersKey: nil,
                               expectedOpenParameterSeparator: "{",
                               expectedCloseParameterSeparator: "}")
    }
    
    func testDefaultForgotPassword() {
        defaultAPIEndpointTest(sut: APIUserForgotPassword.defaultUserForgotPassword(),
                               expectedId: "POST_userforgotpassword",
                               expectedEndPoint: "/user/{email}/forgotpassword",
                               expectedMethod: .post,
                               expectedPermission: .notAuthenticated,
                               expectedParametersKey: ["email"],
                               expectedOpenParameterSeparator: "{",
                               expectedCloseParameterSeparator: "}")
    }
    
    func testDefaultChangePassword() {
        defaultAPIEndpointTest(sut: APIUserChangePassword.defaultUserChangePassword(),
                               expectedId: "POST_userchangepassword",
                               expectedEndPoint: "/user/{email}/changepassword",
                               expectedMethod: .post,
                               expectedPermission: .notAuthenticated,
                               expectedParametersKey: ["email"],
                               expectedOpenParameterSeparator: "{",
                               expectedCloseParameterSeparator: "}")
    }
    
    func defaultAPIEndpointTest(sut: APIEndpoint,
                                expectedId: String,
                                expectedEndPoint: String,
                                expectedMethod: APIHttpMethod,
                                expectedPermission: APIExecutionPermission,
                                expectedParametersKey: [String]?,
                                expectedOpenParameterSeparator: String,
                                expectedCloseParameterSeparator: String) {
        XCTAssertEqual(sut.apiIdentifier, expectedId)
        XCTAssertEqual(sut.endPoint, expectedEndPoint)
        XCTAssertEqual(sut.httpMethod, expectedMethod)
        XCTAssertEqual(sut.apiPermission, expectedPermission)
        XCTAssertEqual(sut.parametersKeys, expectedParametersKey)
        XCTAssertEqual(sut.openParameterSeparator, expectedOpenParameterSeparator)
        XCTAssertEqual(sut.closeParameterSeparator, expectedCloseParameterSeparator)
    }
}

class DefautAPIWithNoParameter: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/type"
    var parametersType: EndopointParametersType = .insideURL
    var parametersKeys: [String]? = nil
    var apiPermission = APIExecutionPermission.notAuthenticated
}

class GetAPIWithNoParameter: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/pippo/getName"
    var httpMethod = APIHttpMethod.post
    var parametersType: EndopointParametersType = .insideURL
    var parametersKeys: [String]? = nil
    
    func setId(identifier: String) {
        apiIdentifier = identifier
    }
}

class GetAPIWithParameter: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/type/{typeId}"
    var httpMethod = APIHttpMethod.post
    var parametersType: EndopointParametersType = .insideURL
    var parametersKeys: [String]? = ["typeId"]
}

class GetAPIWithMultupleParameters: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/type/{typeId}/subtype/{subtypeId}/"
    var httpMethod = APIHttpMethod.post
    var parametersType: EndopointParametersType = .insideURL
    var parametersKeys: [String]? = ["typeId", "subtypeId"]
}


class AuthenticatedGetAPIWithNoParameters: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/type"
    var httpMethod = APIHttpMethod.get
    var apiPermission = APIExecutionPermission.authenticated
    var parametersType: EndopointParametersType = .insideURL
    var parametersKeys: [String]? = nil
}

class GetAPIWithQueryParameter: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/type"
    var httpMethod = APIHttpMethod.post
    var parametersType: EndopointParametersType = .query
    var parametersKeys: [String]? = ["typeId", "aaa"]
}

class GetAPIWithQueryOneParameter: APIEndpoint {
    var apiIdentifier = "aaa"
    var endPoint = "/type"
    var httpMethod = APIHttpMethod.post
    var parametersType: EndopointParametersType = .query
    var parametersKeys: [String]? = ["typeId"]
}
