//
//  DummySerializer.swift
//  NetworkUtilsTests
//
//  Created by Massimo Cesaraccio on 08/04/2020.
//  Copyright © 2020 Longwave. All rights reserved.
//

import Foundation
import LongwaveNetworkUtils

class DummySerializer: SecureSerializer {
    func store(value: String, forKey: String) {
        storage[forKey] = value
    }
    
    func load(_ key: String) -> String? {
        storage[key]
    }
    
    func delete(_ key: String) {
        storage[key] = nil
    }
    
    var storage: [String: String] = [:]
}

