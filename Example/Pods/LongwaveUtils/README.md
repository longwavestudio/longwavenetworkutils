# LongwaveUtils


[![CI Status](https://img.shields.io/travis/mcesaraccio@gmail.com/LongwaveUtils.svg?style=flat)](https://travis-ci.org/mcesaraccio@gmail.com/LongwaveUtils)
[![Version](https://img.shields.io/cocoapods/v/LongwaveUtils.svg?style=flat)](https://cocoapods.org/pods/LongwaveUtils)
[![License](https://img.shields.io/cocoapods/l/LongwaveUtils.svg?style=flat)](https://cocoapods.org/pods/LongwaveUtils)
[![Platform](https://img.shields.io/cocoapods/p/LongwaveUtils.svg?style=flat)](https://cocoapods.org/pods/LongwaveUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LongwaveUtils is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LongwaveUtils'
```

## Author

mcesaraccio@gmail.com, mcesaraccio@gmail.com

## License

LongwaveUtils is available under the MIT license. See the LICENSE file for more info.
