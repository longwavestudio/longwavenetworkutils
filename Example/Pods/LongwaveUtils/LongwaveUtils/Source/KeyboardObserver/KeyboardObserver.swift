//
//  KeyboardObserver.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

public protocol KeyboardObservable {
    var openBlock: ((_ size: CGSize) -> Void)? { get set }
    var closeBlock: ((_ size: CGSize) -> Void)? { get set }
}

/// Observes the keyboard and notifies when it opens and closes.
/// !!! Warning: should be connected on viewDidLoad or view(Will/Did)Appear. If the keyboard is already opened when loaded the result could be not valid.
public final class KeyboardObserver: KeyboardObservable {
    public var openBlock: ((_ size: CGSize) -> Void)?
    public var closeBlock: ((_ size: CGSize) -> Void)?
    private var notificationCenter: NotificationCenter?
    private var keyboardObservers: [NSObjectProtocol] = []
    private var isKeyboardOpen: Bool?
    public var margin: CGFloat = 0

    public init(forNotificationCenter notificationCenter: NotificationCenter = NotificationCenter.default) {
        self.notificationCenter = notificationCenter
        addKeyboardObserver()
    }
    
    deinit {
         removeKeyboardObserver()
    }
    
    /// Adds a keyboard observer  and notify on openBlock closure when the event is triggered
    func addKeyboardObserver() {
        if let o1 = notificationCenter?.addObserver(forName: UIResponder.keyboardWillShowNotification,
                         object: nil,
                         queue: nil, using: { [weak self] notification in
                            guard let self = self else { return }
                            guard !(self.isKeyboardOpen ?? false) else { // Needed on iOs 11 because every time we focus on  password field it call open keyboard again
                                return
                            }
                            self.isKeyboardOpen = true
                            let keyboardSize = self.keyboardSizeFromNotification(notification)
                            self.openBlock?(keyboardSize)
            }) {
            keyboardObservers.append(o1)
        }
        if let o2 = notificationCenter?.addObserver(forName: UIResponder.keyboardWillHideNotification,
                         object: nil,
                         queue: nil, using: { [weak self] notification in
                            guard let self = self else { return }
                            self.isKeyboardOpen = false
                            let keyboardSize = self.keyboardSizeFromNotification(notification)
                            self.closeBlock?(keyboardSize)
            }) {
                keyboardObservers.append(o2)
        }
    }
    
    private func keyboardSizeFromNotification(_ notification: Notification) -> CGSize {
        let frameInfo = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]
        guard let keyboardFrame = (frameInfo as? NSValue)?.cgRectValue  else {
            return .zero
        }
        return sizeByAdding(margin: margin, toFrame: keyboardFrame)
    }
    
    private func sizeByAdding(margin: CGFloat, toFrame frame: CGRect) -> CGSize {
        guard margin != 0 else {
            return frame.size
        }
        return CGSize(width: frame.width, height: frame.height + margin)
    }
    
    /// Removes the keyboard observer
    func removeKeyboardObserver() {
        for observer in keyboardObservers {
            notificationCenter?.removeObserver(observer)
        }
        keyboardObservers.removeAll()
    }
}
