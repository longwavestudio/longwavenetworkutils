//
//  DefaultDrawerContainerViewControllerFactory.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

fileprivate typealias Loader = DefaultStoryboardLoader
fileprivate typealias Menu = DrawerCustomizableMenu

public final class DefaultDrawerContainerViewControllerFactory: DrawerContainerViewControllerFactory {
   
    public init() {}
    
    public func menuContainerViewController() -> UIViewController & MenuDismissableContainer {
        let vc = MenuContainerViewController()
        return vc
    }
    
    public func menuViewController() -> DrawerEventProviderViewController {
        let finder = DrawerStoryboardFinder()
        let currentBundle = Bundle(for: Menu.self)
        let menuId = "DrawerCustomizableMenu"
        let vc: Menu = try! Loader.viewController(with: menuId,
                                                  finder: finder,
                                                  bundle: currentBundle)
        return vc
    }
}
