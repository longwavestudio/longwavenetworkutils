//
//  MenuContainer.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 16/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

final class MenuContainerViewController: UIViewController {
    var notifier: DrawerEventNotifier?
    
    private func addGestureView() -> UIView {
        
        let width = view.bounds.width * (1 - leftMenuScreenCoverage)
        let gestureView = UIView(frame: CGRect(x: view.bounds.width - width,
                                               y: 0,
                                               width: width,
                                               height: view.bounds.height))
        view.addSubview(gestureView)
        return gestureView
    }
    
    private func addGestureRecognizer(to view: UIView) {
        let gestureRecognizer = UITapGestureRecognizer(target: self,
                                                       action: #selector(onTap))
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func onTap() {
        notifier?.fireEvent(.close)
    }
}

extension MenuContainerViewController: MenuDismissableContainer {
    func setup(with notifier: DrawerEventNotifier) {
        self.notifier = notifier
        let gestureView = addGestureView()
        addGestureRecognizer(to: gestureView)
    }
}
