//
//  DrawerEventNotifier.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public typealias OnDrawerEventBlock = (_ event: DrawerEvent) -> Void

public enum DrawerEvent {
    case open(DrawerPosition)
    case close
}

extension DrawerEvent: Equatable {
    public static func == (l: DrawerEvent, r: DrawerEvent) -> Bool {
        switch (l, r) {
        case (.open(_), .open(_)):
            return true
        case (.close, .close):
            return true
        default:
            return false
        }
    }
    
}

/// The Container capable of opening the Drawer should implement this protocol
public protocol DrawerEventSubscribable {
    /// Enables the caller to subscribe to receive drawer events
    /// - Parameter onEventBlock: the block to be called when the event is triggered
    func subscribeForDrawerEvent(onEventBlock: @escaping OnDrawerEventBlock)
}
