//
//  DrawerEventProvider.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 12/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public enum DrawerPosition {
    case left
    case right
}

/// The Interactor implements this protocol to communicate the event to all subscribed listener
public protocol DrawerEventNotifier {
    func fireEvent(_ event: DrawerEvent)
}
