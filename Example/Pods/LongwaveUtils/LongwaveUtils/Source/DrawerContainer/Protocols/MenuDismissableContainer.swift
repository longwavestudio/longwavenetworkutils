//
//  MenuDismissableContainer.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 17/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public protocol MenuDismissableContainer {
    func setup(with notifier: DrawerEventNotifier)
}
