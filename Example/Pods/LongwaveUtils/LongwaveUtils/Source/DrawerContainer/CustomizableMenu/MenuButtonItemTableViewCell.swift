//
//  MenuButtonItemTableViewCell.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 16/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

protocol MenuButtonItem {
    var image: UIImage? { get }
    var title: String { get }
}

final class MenuButtonItemTableViewCell: UITableViewCell {
    static let defaultReuseIdentifier = "MenuButtonItemTableViewCell"
    
    @IBOutlet weak var leadingImage: UIImageView!
    @IBOutlet weak var buttonTitleLabel: UILabel!
    
    func setup(viewModel: MenuButtonItem) {
        leadingImage.image = viewModel.image
        buttonTitleLabel.text = viewModel.title
    }
}
