//
//  DrawerCustomizableMenu.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 13/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public struct DrawerMenuItem {
    var image: UIImage?
    var title: String
    var onItemDidTap: (() -> Void)?
    
    public init(image: UIImage?, title: String, onItemDidTap: (() -> Void)?) {
        self.image = image
        self.title = title
        self.onItemDidTap = onItemDidTap
    }
}

extension DrawerMenuItem: MenuButtonItem {}

public struct DrawerMenuSection {
    var title: String
    var elements: [DrawerMenuItem] = []
    
    public init(title: String, elements: [DrawerMenuItem]) {
        self.title = title
        self.elements = elements
    }
}

public struct DrawerMenuConfiguration {
    var header: (UIView & CloseProvider)?
    var sections: [DrawerMenuSection]
    
    public init(header: (UIView & CloseProvider)?, sections: [DrawerMenuSection]) {
        self.header = header
        self.sections = sections
    }
}

public protocol CloseProvider {
    var onClose: (() -> Void)? { get set }
}

final class DrawerCustomizableMenu: UIViewController {
    
    @IBOutlet private(set) weak var menuTableView: UITableView!
    
    private(set) var drawerInteractor: DrawerEventNotifier?
    private var menu: DrawerMenuConfiguration!
    
    func setup(menu: DrawerMenuConfiguration, interactable: DrawerEventNotifier) {
        self.menu = menu
        self.drawerInteractor = interactable
        var header = menu.header ?? defaultHeaderView()
        header.onClose = { [weak self] in
            self?.drawerInteractor?.fireEvent(.close)
        }
        header.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: 60)
        header.translatesAutoresizingMaskIntoConstraints = true;
        menuTableView.tableHeaderView = header
        menuTableView.tableFooterView = UIView()
    }
    
    private func defaultHeaderView() -> DefaultMenuHeaderView {
        let bundle = Bundle(for: DefaultMenuHeaderView.self)
        let nib = UINib(nibName: "DefaultMenuHeaderView",
                        bundle: bundle )
        let view = nib.instantiate(withOwner: nil, options: nil).first as! DefaultMenuHeaderView
        view.connectButton()
        return view
    }
}

extension DrawerCustomizableMenu: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return menu.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < menu.sections.count else {
            return 0
        }
        return menu.sections[section].elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseId = MenuButtonItemTableViewCell.defaultReuseIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId,
                                                 for: indexPath)
        let menuItem = menu.sections[indexPath.section].elements[indexPath.row]
        (cell as? MenuButtonItemTableViewCell)?.setup(viewModel: menuItem)
        return cell
    }
}

extension DrawerCustomizableMenu: DrawerInteractable {}
extension DrawerCustomizableMenu: CustomizableMenu {}
