//
//  Notifier.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

protocol NotificationSource {
    associatedtype T
    /// Triggers a communication event of type T
    /// - Parameter value: The communication to be performed
    func notify(_ value: T)
}

protocol Subscribable {
    associatedtype T
    /// Subscribes the observer to events. Events will trigger the callback.
    /// - Parameter callback: The callback fired when the event occurred.
    func subscribe(_ callback: @escaping (_ newName: T) -> Void) -> NotificationObserver
}

protocol Unsubscribable: class {
    /// Unsubscribes the observer.
    /// - Parameter observer: the observer to be unsubscribed
    func unsubscribe(observer: NotificationObserver)
}

/// Implementation of a multicast delegate with closures.
public final class Notifier<Item> {
    public typealias Callback = (_ newName: Item) -> Void
    private var map: [Int: Callback] = [:]
    private let queue: DispatchQueue

    public init(queue: DispatchQueue = DispatchQueue.main) {
        self.queue = queue
    }
}

extension Notifier: Subscribable {
    public typealias T = Item
    
    /// Subscribe to the event fired with the notify
    /// - Parameter callback: the callback to be fired whe the event occurred
    public func subscribe(_ callback: @escaping Callback) -> NotificationObserver {
        let newKey = NotificationObserver(notifier: self)
        queue.async {
            self.map[newKey.hashValue] = callback
        }
        return newKey
    }
}

extension Notifier: Unsubscribable {
    /// Unsubscribe from the event.
    /// - Parameter observer: the observer to be unsubscribed
    public func unsubscribe(observer: NotificationObserver) {
        let hash = observer.hashValue
        queue.async {
            self.map[hash] = nil
        }
    }
}

extension Notifier: NotificationSource {
    
    /// Fires an event that will trigger all the callbacks associated.
    /// - Parameter value: The value associated to the event.
    public func notify(_ value: T) {
        queue.async {
            self.map.values.forEach { callback in
                callback(value)
            }
        }
    }
}

// An object to keep track of the subscription.
// Please note that, if this object is not retained it will unsubscribe automatically in deinit.
public final class NotificationObserver {
    
    fileprivate let uuid = UUID()
    private weak var notifier: Unsubscribable?
    
    init(notifier: Unsubscribable) {
        self.notifier = notifier
    }
    
    deinit {
        notifier?.unsubscribe(observer: self)
    }
}

extension NotificationObserver: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(uuid.hashValue)
    }
}

public func ==(lhs: NotificationObserver, rhs: NotificationObserver) -> Bool {
    return lhs.uuid == rhs.uuid
}
