//
//  AlertControllerFinder.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 09/03/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

final class AlertControllerFinder: StoryboardFinder {
    let storyboardName = "Alert"
    
    func viewControllers(inStoryboard id: String) -> [String] {
        return ["CustomAlertViewController"]
    }    
}
