//
//  ColorComponents.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 10/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

/// Contains data about RGBA Channels in UIColor
public struct ColorComponents {
    var red: CGFloat
    var green: CGFloat
    var blue: CGFloat
    var alpha: CGFloat
    
    public init(red: CGFloat,
                green: CGFloat,
                blue: CGFloat,
                alpha: CGFloat) {
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha
    }
    
    /// Returns the UIColor defined by the RGBA values
    public func uiColor() -> UIColor {
        return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha / 255.0)
    }
}
