//
//  Serializer.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

public protocol Serializer {
    /// Retrieves an object given the serialization key
    func object(forKey defaultName: String) -> Any?
    /// Sets the object to be serialized using the identification key
    func set(_ value: Any?, forKey defaultName: String)
}
