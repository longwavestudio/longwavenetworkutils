//
//  UIView+Elevation.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

@IBDesignable
public extension UIView {
    
    @IBInspectable
    var elevation: Double {
        get {
            return 0.0
        }
        set {
            elevate(elevation: newValue, opacity: 1.0)
        }
    }
    
    /// Draws a shadow around a view giving the user a sense of depth among the views in the view hierarchy
    /// - Parameters:
    ///   - elevation: the value of the elevation
    ///   - opacity: the opacity to be used
    func elevate(elevation: Double, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOpacity = opacity
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: elevation)
        layer.shadowRadius = CGFloat(elevation)
    }
}
