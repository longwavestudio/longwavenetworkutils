//
//  ActivityHandler.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 10/01/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

/// Abstracts the activity indicator hide/show mechanism.
public protocol ActivityHandler {
    var activityIndicator: UIActivityIndicatorView? { get }
    /// Shows the activity indicator view and start the animation
    func startActivity()
    /// Hides the activity indicator view and stop the animation
    func stopActivity()
}

public extension ActivityHandler {
    
    func startActivity() {
        activityIndicator?.startAnimating()
    }
    
    func stopActivity() {
        activityIndicator?.stopAnimating()
    }
}
