//
//  DefaultTimer.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 06/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

private enum State {
    case suspended
    case resumed
}

public final class DefaultTimer {
    private(set) var timeInterval: TimeInterval = 0.0
        
    public var eventHandler: (() -> Void)?
    
    private var state: State = .suspended
    private var timer: DispatchSourceTimer
    
    public init() {
        timer = DispatchSource.makeTimerSource()
        timer.setEventHandler(handler: { [weak self] in
            DispatchQueue.main.async {
                self?.eventHandler?()
            }
        })
    }
    
    deinit {
        timer.setEventHandler(handler: nil)
        timer.cancel()
        resume(timeInterval: timeInterval)
        eventHandler = nil
    }    
}

extension DefaultTimer: LWTimer {
    public func resume(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
        timer.schedule(deadline: .now() + self.timeInterval)
        if state == .resumed { return }
        state = .resumed
        timer.resume()
    }
    
    public func suspend() {
        if state == .suspended { return }
        state = .suspended
        timer.suspend()
    }
}
