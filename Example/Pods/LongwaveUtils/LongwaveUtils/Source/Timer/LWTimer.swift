//
//  LWTimer.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 06/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public protocol LWTimer: class {
    var eventHandler: (() -> Void)? { get set }
    func resume(timeInterval: TimeInterval)
    func suspend()
}
