//
//  DefaultTabFlowController.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 04/05/2020.
//

import UIKit

public final class DefaultTabFlowController {
    
    private var dependency: TabFlowDependency
    public weak var tabController: UITabBarController?
    
    private weak var currentEmbedder: UIViewController?
    private weak var menu: MenuFlow?
    
    public init(dependency: TabFlowDependency) {
        self.dependency = dependency
    }
}

extension DefaultTabFlowController: TabFlowController {
    
    public func embed(in viewController: UIViewController,
                      onCompletion: @escaping (_ info: [String: Any]) -> Void) {
        self.currentEmbedder = viewController
        let tab = UITabBarController()
        self.tabController = tab
        self.menu = dependency.menu
        viewController.embed(viewController: tab)
        embedContainers(in: tab,
                        menuFlowController: menu,
                        onCompletion: onCompletion)
    }
    
    public func disembed() {
        if let tab = tabController {
            currentEmbedder?.removeEmbeddedViewController(embeddedViewController: tab)
        }
        
    }
    
    private func embedContainers(in tabBarController: UITabBarController,
                                 menuFlowController: MenuFlow?,
                                 onCompletion: @escaping (_ info: [String: Any]) -> Void) {
        var controllers: [UIViewController] = []
        for flow in dependency.flows {
            let controller = UIViewController()
            controllers.append(controller)
            flow.embed(in: controller, onCompletion: onCompletion)
            subscribe(flow: flow, for: menuFlowController)
        }
        tabBarController.setViewControllers(controllers, animated: false)
        for i in 0..<dependency.flows.count {
            let flow = dependency.flows[i]
            let controller = tabBarController
            controller.tabBar.items?[i].image = flow.icon
            controller.tabBar.items?[i].selectedImage = flow.iconSelected
            controller.tabBar.items?[i].title = flow.title
        }
    }
    
    private func subscribe(flow: TabFlowEmbeddable, for menuFlowController: MenuFlow?) {
        guard let menuFlow = menuFlowController else {
            return
        }
        flow.subscribe { [weak menuFlow, weak self] type, callback in
            guard let self = self else { return }
            menuFlow?.setup(type: type,
                            actionBlock: { [weak self] (action, tab) in
                                self?.tabController?.selectedIndex = tab
                                print("should call action: \(action)")
                                callback(action)
            })
            if let tab = self.tabController {
                menuFlow?.embedMenu(in: tab,
                                    animated: true,
                                    type: type,
                                    onClose: { [weak self] in
                                        self?.menu = nil
                })
            }
        }
    }
}
