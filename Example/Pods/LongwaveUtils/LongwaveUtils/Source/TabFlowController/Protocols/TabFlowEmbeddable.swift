//
//  TabFlowEmbeddable.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 05/05/2020.
//

import UIKit

public typealias MenuEventBlock = (_ action: String, _ tab: Int) -> Void
public typealias MenuFlowEventBlock = (_ action: String) -> Void

public protocol TabFlowEmbeddable: class {
    var icon: UIImage { get }
    var iconSelected: UIImage { get }
    var title: String { get }
    func embed(in viewController: UIViewController, onCompletion: @escaping (_ info: [String: Any]) -> Void)
    func subscribe(onMenuOpen: @escaping (_ menuType: String?, _ onMenuEvent: @escaping MenuFlowEventBlock) -> Void)
}
