//
//  MenuFlow.swift
//  LongwaveUtils
//
//  Created by Alessio Bonu on 05/05/2020.
//

import UIKit

public protocol MenuFlow: class {
    func embedMenu(in viewController: UIViewController, animated: Bool, type: String?, onClose: @escaping () -> Void)
    
    func setup(type: String?, actionBlock block: @escaping MenuEventBlock)
}
