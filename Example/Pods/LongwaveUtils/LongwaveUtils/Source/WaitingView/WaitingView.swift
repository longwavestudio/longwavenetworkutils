//
//  WaitingView.swift
//  StreamagoSocial
//
//  Created by Massimo Cesaraccio on 15/06/16.
//  Copyright © 2016 Streamago Inc. All rights reserved.
//

import UIKit

public extension UIView {
    private static func defaultAnimationDuration() -> TimeInterval {
        return 0.2
    }
    
    final func addFullscreenView(_ view: UIView, animated: Bool = true) {
        addSubview(view)
        view.frame = bounds
        constrainView(view, showAnimated: animated)
    }
    
    fileprivate func constrainView(_ view: UIView, showAnimated: Bool) {
        view.translatesAutoresizingMaskIntoConstraints = false
        // create constraints to keep the loading view attached to borders
        let views = ["loadingView" : view ]
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[loadingView]-0-|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[loadingView]-0-|", options: [], metrics: nil, views: views)
        addConstraints(constraints)
        
        guard showAnimated else {
            return
        }
        view.alpha = 0
        UIView.animate(withDuration: UIView.defaultAnimationDuration()) {
            view.alpha = 1.0
        }
    }
}

/// Utility view that shows a translucent blocking layer and an activity indicator.
public final class WaitingView: UIView {
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    private final class func animationDuration() -> TimeInterval {
        return 0.25
    }
    
    private final class func instanceFromNib() -> WaitingView {
        return UINib(nibName: "WaitingView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! WaitingView
    }
    
    /// Adds a new WaitingView instance to the view.
    ///
    /// - Parameters:
    ///   - view: the view to which the instance shuold be added
    ///   - animated: _true_ to perform an animated insertion.
    /// - Returns: the newly added waiting view.
    fileprivate final class func addInstanceToView(view: UIView,
                                                   animated: Bool,
                                                   blurred: Bool) -> WaitingView {
        let newWaitingView = WaitingView(frame: view.bounds)
        if blurred {
            addBlurToView(newWaitingView)
        }
        addSpinnerToView(newWaitingView)
        view.addFullscreenView(newWaitingView, animated: animated)
        newWaitingView.spinner.startAnimating()
        return newWaitingView
    }
    
    private static func addBlurToView(_ waitingView: WaitingView) {
        let blur = UIBlurEffect(style: .prominent)
        let effectView = UIVisualEffectView(effect: blur)
        waitingView.addFullscreenView(effectView)
    }
    
    private static func addSpinnerToView(_ waitingView: WaitingView) {
        let spinner = UIActivityIndicatorView(style: .gray)
        waitingView.spinner = spinner
        waitingView.addSubview(spinner)
        let views = ["superview": waitingView, "spinner": spinner]
        let constraintsV = NSLayoutConstraint.constraints(withVisualFormat: "V:[superview]-(<=1)-[spinner]",
                                                          options: NSLayoutConstraint.FormatOptions.alignAllCenterX,
                                                          metrics: nil,
                                                          views: views)
        let constraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:[superview]-(<=1)-[spinner]",
                                                          options: NSLayoutConstraint.FormatOptions.alignAllCenterY,
                                                          metrics: nil,
                                                          views: views)
        waitingView.addConstraints(constraintsV + constraintsH)
        spinner.translatesAutoresizingMaskIntoConstraints = false
    }
    
    /// makes the spinner style small gray
    public final func gray() -> WaitingView {
        spinner.style = .gray
        backgroundColor = UIColor.white
        return self
    }
    
    /// makes the spinner style white large
    public final func white() -> WaitingView {
        spinner.style = .whiteLarge
        backgroundColor = UIColor.black.withAlphaComponent(0.4)
        return self
    }
    
    /// Performs an animated removal.
    /// Removes the view from its superview
    ///
    /// - Parameter animated: _true_ to perform an animated removal.
    public func remove(animated: Bool) {
        let duration: TimeInterval = animated ? WaitingView.animationDuration() : 0
        UIView.animate(withDuration: duration, animations: { self.alpha = 0 }, completion: { _ in
            self.removeFromSuperview()
        })
    }
}

// MARK: - Default UIView behaviour to add and remove WaitingView instances.
public extension UIView {
    private final func waitingViewTag() -> Int {
        return 9191
    }
    
    /// Returns any existing waiting view.
    func waitingView() -> WaitingView? {
        return self.viewWithTag(waitingViewTag()) as? WaitingView
    }
    
    /// Adds a waiting view if not already present, returns the new or existing view.
    ///
    /// - Parameter animated: _true_ to perform an animated insertion.
    /// - Returns: the waiting view that for either found or added.
    @discardableResult
    final func addWaitingView(animated: Bool, blurred: Bool = false) -> WaitingView? {
        var instance = waitingView()
        if instance == nil {
            instance = WaitingView.addInstanceToView(view: self, animated: animated, blurred: blurred)
            instance?.tag = waitingViewTag()
        }
        return instance
    }
    
    /// Removes any existing waiting view.
    ///
    /// - Parameter animated: _true_ to perform an animated removal.
    /// - Returns: the removed waiting view, if one could be found.
    @discardableResult
    final func removeWaitingView(animated: Bool) -> WaitingView? {
        guard let waitingView = self.waitingView() else {
            return nil
        }
        waitingView.remove(animated: animated)
        return waitingView
    }
}
