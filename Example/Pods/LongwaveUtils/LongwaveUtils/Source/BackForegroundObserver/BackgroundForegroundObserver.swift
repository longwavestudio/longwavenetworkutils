//
//  BackgroundForegroundObserver.swift
//  LongWaveUtilsTests
//
//  Created by Alessio Bonu on 02/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public protocol BackgroundForegroundObserver {
    func startObserving(onBackground: (() -> Void)?,
                        onForeground: (() -> Void)?,
                        onActive: (() -> Void)?)
}
