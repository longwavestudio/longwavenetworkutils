//
//  Localizable.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

/// A protocol to decouple from standard localization
public protocol Localizable {
    func localizedString() -> String
    func localizedString(in bundle: Bundle) -> String
}

extension Localizable {
    public func localizedString() -> String {
        return localizedString(in: Bundle.main)
    }
}
