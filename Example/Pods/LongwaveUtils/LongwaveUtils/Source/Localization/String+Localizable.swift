//
//  String+Localizable.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//


import Foundation

extension String: Localizable {
    public func localizedString(in bundle: Bundle) -> String {
        return NSLocalizedString(self, bundle: bundle, comment: self)
    }
}
