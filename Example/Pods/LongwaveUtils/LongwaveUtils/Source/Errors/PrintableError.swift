//
//  PrintableError.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

/// Represents an error that contains the strings representing the human readable representation of the info to be shown to the user when the error occurs.
public protocol PrintableError {
    /// The title of alert to be shown
    func getErrorTitle() -> String?
    /// The message to be shown
    func getErrorMessage() -> String
    /// Utils method to show a simple alert with all the text set
    func showError(on presenter: UIViewController?, actions: [UIAlertAction]) -> UIAlertController?
}

extension PrintableError {
    public func getErrorTitle() -> String? {
        return nil
    }
    
    @discardableResult
    public func showError(on presenter: UIViewController?, actions: [UIAlertAction] = []) -> UIAlertController? {
        guard let presenter = presenter else {
            return nil
        }
        let controller = UIAlertController(title: getErrorTitle(),
                                           message: getErrorMessage(),
                                           preferredStyle: .alert)
        if actions.isEmpty {
            let defaultAction = UIAlertAction(title: "Ok",
                                              style: .default,
                                              handler: nil)
            controller.addAction(defaultAction)
        } else {
            for action in actions {
                controller.addAction(action)
            }
        }
        presenter.present(controller, animated: true, completion: nil)
        return controller
    }
}
