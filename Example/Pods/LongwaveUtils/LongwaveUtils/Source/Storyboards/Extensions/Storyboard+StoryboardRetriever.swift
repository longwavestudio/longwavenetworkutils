//
//  Storyboard+StoryboardRetriever.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

extension UIStoryboard: StoryboardRetriever {
    public func viewController<ViewController>(identifier: String) -> ViewController where ViewController : UIViewController {
        if #available(iOS 13.0, *) {
            return instantiateViewController(identifier: identifier)
        } else {
            return instantiateViewController(withIdentifier: identifier) as! ViewController
        }
    }
    
    public func viewController<V, I: RawRepresentable>(identifier: I) -> V where V : UIViewController, I.RawValue == String {
        return viewController(identifier: identifier.rawValue)
    }
}
