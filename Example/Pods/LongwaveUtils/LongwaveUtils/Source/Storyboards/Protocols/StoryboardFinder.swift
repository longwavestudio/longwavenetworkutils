//
//  StoryboardFinder.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

public enum StoryboardIdError: Error {
    case notFound
}

/// Decouples the UIstoryboard class
public protocol StoryboardRetriever {
    /// Retrieves a UIViewController of type 'ViewController' given the view controller identifier.
    func viewController<ViewController>(identifier: String) -> ViewController where ViewController : UIViewController
}

/// Allows to retrieve storyboard information from viewController Id
/// Implements this protocol to specify all the ViewControllers Id contained in a storyboard
/// The Implementation will be used to get the ritght storyboard Id given the viewControllerId
public protocol StoryboardFinder: class {
            
    var storyboardName: String { get }
    
    /// Retrieves the storyboard Id from the viewController Id
    /// - Parameter viewControllerId: the viewController Id
    func storyboardId(for viewControllerId: String) throws -> String
    
    /// Returns the list of all the viewControllers contained inside the Storyboard
    /// - Parameter id: the id of the storyboard
    func viewControllers(inStoryboard id: String) -> [String]
    
    /// Get the instanse of a storyboard retriever
    /// - Parameters:
    ///   - id: the id of the storyboard
    ///   - bundle: the bundle used to retrieve the storyboard
    func storyboard(for id: String, bundle: Bundle?) -> StoryboardRetriever
}
 
public extension StoryboardFinder {
    func storyboardId(for viewControllerId: String) throws -> String {
        guard viewControllers(inStoryboard: storyboardName).contains(viewControllerId) else {
            throw StoryboardIdError.notFound
        }
        return storyboardName
    }
}

extension StoryboardFinder {
    public func storyboard(for id: String, bundle: Bundle?) -> StoryboardRetriever {
        return UIStoryboard(name: id, bundle: bundle)
    }
}
