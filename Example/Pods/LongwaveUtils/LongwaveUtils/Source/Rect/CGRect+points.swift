//
//  CGRect+points.swift
//  LongWaveUtils
//
//  Created by Alessio Bonu on 07/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

/// From CGRect to paths
extension CGRect {
    /// Returns all the GCPoints that describe the square on a path
    /// - Parameter box: the rect to be processed
    func getBoxPoints() -> [CGPoint] {
        let originX = origin.x
        let originY = origin.y
        let boxWidth = size.width
        let boxHeight = size.height
        let boxPoints = [CGPoint(x: originX,
                                 y: originY),
                         CGPoint(x: originX + boxWidth,
                                 y: originY),
                         CGPoint(x: originX + boxWidth,
                                 y: originY + boxHeight),
                         CGPoint(x: originX,
                                 y: originY + boxHeight)]
        return boxPoints
    }
}
